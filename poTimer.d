import std.stdio,
  core.thread,
  std.process,
  std.system;

void main()
{
  float tmp = 0;
  long secs = 0;
  string clear;
  string shutdown;

  if(os == OS.linux)
    {
      clear = "clear";
      shutdown = "poweroff";
    }
  else if( os == OS.win32 || os == OS.win64)
    {
      clear = "cls";
      shutdown = "shutdown -h";
    }

  write("Enter hours: ");
  readf(" %s", &tmp);
  secs += tmp * 60 * 60;
  write("Enter minutes: ");
  readf(" %s", &tmp);
  secs += tmp * 60;
  system(clear);
  writeln("Power Off");
  writeln(secs/(60*60), " h, ", (secs % 3600)/60 , " m left");
  while(secs > 60)
    {
      Thread. sleep( dur!("seconds")( 60 ) );
      secs -= 60;
      system(clear);
      writeln("Power Off");
      writeln(secs/(60*60), " h, ", (secs % 3600)/60 , " m left");
    }
  Thread. sleep( dur!("seconds")( secs ) );
  system(shutdown);
}
